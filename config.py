from helper.file import File
from os import makedirs, getcwd
from os.path import isdir
import json

default_content = {
    "component_name": "NewComponent",
    "output_directory": "..\..\source\stories\\",
    "template_version": "storybook-typescript"
}

def get_config_folder():
    if not isdir(getcwd() + "\\config"):
        print("[Component Templates] Create Config Directory")
        makedirs(getcwd() + "\\config")


def update_config(json_object):
    value = input("[Input Prompt] Update Component Name (Leave blank to skip)\n[Input --> Default]: "+default_content["component_name"]+"\n[Input --> Current]: "+json_object["component_name"]+"\n[Input --> New]: ")
    if value != "" and value != " ":
        json_object["component_name"] = value
    value = input("[Input Prompt] Update Output Directory (Leave blank to skip)\n[Input --> Default]:"+default_content["output_directory"]+"\n[Input --> Current]: "+json_object["output_directory"]+"\n[Input --> New]: ")
    if value != "" and value != " ":
        json_object["output_directory"] = value
    value = input("[Input Prompt] Update Template Version (Leave blank to skip)\n[Input --> Default]:"+default_content["template_version"]+"\n[Input --> Current]: "+json_object["template_version"]+"\n[Input --> New]: ")
    if value != "" and value != " ":
        json_object["template_version"] = value
    return json_object;


def validate_config(json_object):
    print("[Component Templates] Validating Configuration!")
    if json_object["template_version"] != "storybook" and json_object["template_version"] != "storybook-typescript" and json_object["template_version"] != "react" and json_object["template_version"] != "react-typescript":
        print("[Warning] Template version does not exist yet. Constructing a new Component may be impossible!")
        return False;
    else:
        print("[Test] Template version does exist!")
    print("[Component Templates] Configuration is valid!")
    return True;


def save_config(json_object, default_params_file):
    with open(default_params_file.name + default_params_file.extension, "w+") as jsonFile:
        print("[Component Templates] Saving Changes")
        jsonFile.seek(0)
        json.dump(json_object, jsonFile, indent=4, sort_keys=True)
        jsonFile.truncate()


def main():
    print("[Component Templates] Configuration Script started")

    
        
    restore_default = input("[Input] Restore to default configuration? [Y/N]")
    get_config_folder()
    default_params_file = File("config/default_params", ".json")
    if default_params_file.doesExist() == False or restore_default.lower() == "y" or restore_default.lower() == "yes":
        save_config(default_content, default_params_file)
    
    if restore_default.lower() != "y" and restore_default.lower() != "yes":
        default_params_file = File("config/default_params", ".json")
        if default_params_file.doesExist():
            json_object = json.loads(default_params_file.read())
            json_object = update_config(json_object)
            if validate_config(json_object):
                save_config(json_object, default_params_file)
            else:
                print("[Component Templates] Configuration is invalid!")
                print("[Component Templates] Changes will not be saved!")
    else:
        print("[Component Templates] Configuration Restored")

    print("[Component Templates] Configuration Script ended")
            

        
if __name__ == "__main__":
    main()
