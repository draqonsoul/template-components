import React from "react"
import "./ComponentName.scss";

const ComponentName = ({ className }) => {
	
	return (
		<div className={className ? "componentName "+className : "componentName"}>
			Hello World
		</div>
  );
}

export default ComponentName