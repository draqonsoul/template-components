import React from "react";
import { ComponentNameProps } from "./ComponentName.types";
import "./ComponentName.scss";

const ComponentName: React.FC<ComponentNameProps> = ({ className }) => {
	
	return (
		<div className={className ? "componentName "+className : "componentName"}>
			Hello World
		</div>
  );
}

export default ComponentName;