import React from "react"
import "./ComponentName.scss";

interface ComponentNameProps {
	className?: string;
}

const ComponentName: React.FC<ComponentNameProps> = ({ className }) => {
	
	return (
		<div className={className ? "componentName "+className : "componentName"}>
			Hello World
		</div>
  );
}

export default ComponentName