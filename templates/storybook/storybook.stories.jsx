import React from "react";
import ComponentName from './ComponentName';

export default {
  title: "ComponentName"
};

export const Default = () => <ComponentName />;