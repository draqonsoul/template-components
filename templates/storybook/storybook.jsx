import React from "react";
import "./ComponentName.scss";

const ComponentName = ({ className }) => {
	
	return (
		<div className={className ? "componentName "+className : "componentName"}>
	
		</div>
  );
}

export default ComponentName;