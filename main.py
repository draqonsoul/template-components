from helper.file import File
from helper.stringreplacer import StringReplacer
from os import makedirs, getcwd
from os.path import isdir
from argparse import ArgumentParser
import json


def main():
    print("[Component Templates] Main Script Started")

    default_params_file = File("config/default_params", ".json")
    if default_params_file.doesExist():
        json_object = json.loads(default_params_file.read())
        parser = ArgumentParser()
        parser.add_argument("-c", "--component", dest="componentName", help="", default=json_object["component_name"])
        parser.add_argument("-p", "--path", dest="outputPath", help="", default=json_object["output_directory"])
        parser.add_argument("-t", "--template", dest="templateType", help="", default=json_object["template_version"])
        args = parser.parse_args()

    if(args.templateType == "react"):
        templateFiles = [
            {"id": 0, "name": "react", "extension": ".jsx"}, 
            {"id": 1, "name": "react", "extension": ".scss"}
        ]
    if(args.templateType == "react-typescript"):
        templateFiles = [
            {"id": 0, "name": "react", "extension": ".tsx"}, 
            {"id": 1, "name": "react", "extension": ".scss"}
        ]
    if(args.templateType == "storybook"):
        templateFiles = [
            {"id": 0, "name": "storybook", "extension": ".jsx"}, 
            {"id": 1, "name": "storybook", "extension": ".stories.jsx"},
            {"id": 2, "name": "storybook", "extension": ".scss"}
        ]
    if(args.templateType == "storybook-typescript"):
        templateFiles = [
            {"id": 0, "name": "storybook", "extension": ".tsx"}, 
            {"id": 1, "name": "storybook", "extension": ".stories.tsx"},
            {"id": 2, "name": "storybook", "extension": ".types.ts"},
            {"id": 3, "name": "storybook", "extension": ".scss"}
        ]
    if args.templateType == "react" or args.templateType == "react-typescript" or args.templateType == "storybook" or args.templateType == "storybook-typescript":
        for mappedFile in templateFiles:
            templateFile = File("templates/"+args.templateType+"/"+mappedFile["name"], mappedFile["extension"])
            if templateFile.doesExist():
                data = templateFile.read()
                data = StringReplacer(data, args.componentName, "ComponentName", strict=False)
                data = StringReplacer(data, args.componentName[0].lower() + args.componentName[:1], "ComponentName", strict=False)
                try:
                    if not isdir(getcwd() + "\\" + args.outputPath + "\\" + args.componentName):
                        makedirs(getcwd() + "\\" + args.outputPath + "\\" + args.componentName)
                    outputFile = File(args.outputPath + args.componentName + "/" +  args.componentName, mappedFile["extension"])
                    outputFile.write(data)
                except:
                    print("[ERROR] The specified output path (-p|--path) does not exist!")
                    break;
            else:
                print("[ERROR] The specified template (-t|--template) does not exist!")
                break;

    print("[Component Templates] Main Script Ended")


if __name__ == "__main__":
    main()
